## Tribe's Kitchen

Tribe's Kitchen is a mini project for our tribe. The main idea of this project is that our people can let the kitchen administrator knows if they are going to eat.

---

## Technologies

Full-stack JavaScript: We use Node.js to power our backend and React to develop our frontend. Almost all of the code you will touch in this project will be JavaScript.

Here is a list of all the big technologies we use:

*Node.js:* JavaScript server runtime

*MongoDB:* Data Storage

*Express.js:* API service framework

*React:* Frontend framework

*CSS:* Cascading Style Sheets


---

## Installation

**Requirements**
MongoDB
Nodejs v10

---

## Setup
Install all the required dependencies running the command: npm install in the terminal.

---

## How to Run this app

FrontEnd: npm start <<running on localhost:8080>>
BackEnd: node server.js <<running on localhost:3000>>

