import React, { Component } from "react";
import ReactDOM from "react-dom";
import Input from "../../components/input/index.jsx";
class Form extends Component {
  constructor() {
    super();
    this.state = {
      title: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }
  render() {
    const { title } = this.state;
    return (
      <form id="article-form">
        <Input
          text="title"
          label="title"
          type="text"
          id="title"
          value={title}
          handleChange={this.handleChange}
        />
      </form>
    );
  }
}
export default Form;

const wrapper = document.getElementById("create-article-form");
wrapper ? ReactDOM.render(<Form />, wrapper) : false;
