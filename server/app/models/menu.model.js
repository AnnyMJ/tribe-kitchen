const mongoose = require('mongoose');

const MenuSchema = mongoose.Schema({
    title: String,
    content: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Menu', MenuSchema);
