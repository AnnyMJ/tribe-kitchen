module.exports = (app) => {
    const menus = require('../controllers/menu.controller.js');

    // Create a new Menu
    app.post('/menus', menus.create);

    // Retrieve all Menus
    app.get('/menus', menus.findAll);

    // Retrieve a single Menu with menuId
    app.get('/menus/:menuId', menus.findOne);

    // Update a Menu with menuId
    app.put('/menus/:menuId', menus.update);

    // Delete a Menu with menuId
    app.delete('/menus/:menuId', menus.delete);
}
